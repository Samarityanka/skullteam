import socket
import sys
import time
import serial
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import (QWidget, QPushButton, QLineEdit,QSlider,
    QInputDialog, QApplication)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *
from PyQt5.QtGui import QBrush, QColor, QPainter, QPen, QPolygon
from PyQt5.QtCore import (pyqtProperty, pyqtSignal, pyqtSlot, QPoint, QSize,
        Qt, QTime, QTimer)

class ex11(QtWidgets.QWidget):
    def __init__(self, parent=None):
        # Передаём ссылку на родительский элемент и чтобы виджет
        # отображался как самостоятельное окно указываем тип окна
        super().__init__(parent, QtCore.Qt.Window)
        self.setWindowTitle("Программа с кнопками")
        self.resize(650, 500)
        self.build()
    def build(self):
        self.but1 = QtWidgets.QPushButton('Отправить роботу', self)
        self.but1.setGeometry(QtCore.QRect(30, 20, 120, 40))
        self.but1.clicked.connect(self.send_1)

        self.but2 = QtWidgets.QPushButton('Open', self)
        self.but2.setGeometry(QtCore.QRect(30, 120, 120, 40))


        self.but3 = QtWidgets.QPushButton('Close', self)
        self.but3.setGeometry(QtCore.QRect(180, 120, 120, 40))


        self.but4 = QtWidgets.QPushButton('Приём', self)
        self.but4.setGeometry(QtCore.QRect(320, 120, 120, 40))


        self.but5 = QtWidgets.QPushButton('Подключиться', self)
        self.but5.setGeometry(QtCore.QRect(180, 180, 120, 40))
        self.but5.clicked.connect(self.op)

        self.but6 = QtWidgets.QPushButton('Закрыть', self)
        self.but6.setGeometry(QtCore.QRect(320, 180, 120, 40))
        self.but6.clicked.connect(self.cl)

        self.but7 = QtWidgets.QPushButton('Ответ датчика', self)
        self.but7.setGeometry(QtCore.QRect(460, 180, 120, 40))
  

        self.comport=["COM1", "COM2", "COM3", "COM4", "COM5", "COM6","COM7","COM8","COM9","COM10","COM11","COM12"]
        
        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setGeometry(QtCore.QRect(30, 220, 120, 40))
        self.slider.setFocusPolicy(Qt.NoFocus)
        

        #127.0.0.1
        #172.31.1.147
        self.textEdit1 = QtWidgets.QTextEdit(self)
        self.textEdit1.setGeometry(QtCore.QRect(100, 300, 300, 80))

        self.label1 = QtWidgets.QLabel(self)
        self.label1.setGeometry(QtCore.QRect(320, 216, 236, 50))
        self.label1.setText('Убедитесь что сервер\n на роботе открыт')
        
        self.label3 = QtWidgets.QLabel(self)
        self.label3.setGeometry(QtCore.QRect(200, 16, 236, 50))
        self.label3.setText('Убедитесь что сервер\n на роботе открыт')


    def cl(self):
        self.label1.setText('close')
    def op(self):
        self.label1.setText('open')
    def send_1(self):
        self.label1.setText('send')

       
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = ex11()
    window.show()
    sys.exit(app.exec_())
